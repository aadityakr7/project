<?php
// Loading config file
require("./../../../resources/config.php");
// Creating connection to the database
require("./../../includes/connect_db.php");
// Including functions
require("./../../includes/functions.php");
// Including the register class
require("register_class.php");
//echo "test register";
?>
<?php
// Retreiving the values
$fname = $_POST['fname']; //echo $fname;
$gender = $_POST['gender']; //echo $gender;
$uname = $_POST['uname']; //echo $uname;
$email = $_POST['email']; //echo $email;
$password = $_POST['password']; //echo $password;
$hashed_password = sha1($password); //echo $hashed_password;
// Extra fields to be inserted
$token = md5($uname);
$verify_flag = 0;

$obj_func = new userFunctions($db);
$exists = $obj_func->user_exists($uname);
//print_r($exists);
if ($exists == true) {
	echo "Username already exists, please choose some other username";
}else {
	// Do register
	$obj_reg = new Register($db);
	$check = $obj_reg->registerUser($fname,$gender,$uname,$email,$hashed_password,$token,$verify_flag);
	if ($check == true) {
		//$obj_reg->regMail($email,$token);
		header("Location: ./../../views/login/login.php");
	} else {
		//
	}
}
?>