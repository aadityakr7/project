<?php
// Login class
class Login
{
	private $db_conn;

	function __construct($db)
	{
		// constructor
		$this->db_conn = $db;
	}

	public function loginUser($uname,$hashed_password)
	{
		$qry = "select uname,hashed_password,verify_flag from users";
		$res = $this->db_conn->query($qry);
		$flag = 0;
		while ($row = $res->fetch_assoc()) {
			// Checking whether user input is valid or not
			if ($uname == $row['uname'] && $hashed_password == $row['hashed_password']) {
				if ($row['verify_flag'] ==1) {
					$flag = 1;
					return $flag;
				}
			}
		}
		if ($flag ==0) {
			//echo "<script>alert('Incorrect username/password combination')</script>";
			return $flag;
		}
	}

}
?>