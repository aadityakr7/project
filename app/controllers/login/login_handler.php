<?php
// Loading config file
require("./../../../resources/config.php");
// Creating connection to the database
require("./../../includes/connect_db.php");
// Including functions
require("./../../includes/functions.php");
// Including the login class
require("login_class.php");
//echo "test login";
?>
<?php
// Retrieving the values
$uname = $_POST['uname']; //echo $uname;
$password = $_POST['password']; //echo $password;
$hashed_password = sha1($password);

$obj_func = new userFunctions($db);
$exists = $obj_func->user_exists($uname);
if ($exists != true) {
	echo "User doen't exist";
} else {
	// Do login
	$obj_login = new Login($db);
	$check = $obj_login->loginUser($uname,$hashed_password);
	//echo $check;
	if ($check == 1) {
		//
		header("Location: ./../../views/home.php");
	} else {
		echo "Incorrect username/password combination";
	}
}
?>