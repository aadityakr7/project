<html>
	<head>
		<title>Registration page</title>
		<link rel="stylesheet" href="./../../../assets/stylesheets/style.css" type="text/css" />
	</head>

	<body>
		<?php include_once("./../../includes/header.php"); ?>
		<fieldset>
			<legend>Registration</legend>
			<form method="POST" action="./../../controllers/register/register_handler.php">
				<table cellspacing="5px">
					<tr>
						<td>Full Name: </td>
						<td><input type="text" name="fname" maxlength="50" required /></td>
					</tr>
					<tr>
					<tr>
						<td>Gender: </td>
						<td><input type="radio" name="gender" value="Male" required>Male
							<input type="radio" name="gender" value="Female" required>Female</td>
					</tr>
						<td>Username: </td>
						<td><input type="text" name="uname" maxlength="25" required /></td>
					</tr>
					</tr>
						<td>Email: </td>
						<td><input type="email" name="email" maxlength="50" required /></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><input type="password" name="password" maxlength="25" required /></td>
					</tr>
					<tr>
						<td align="right" colspan="2"><input type="submit" value="Register" /></td>
					</tr>
				</table>
			</form>
		</fieldset>
		<?php include_once("./../../includes/footer.php"); ?>
	</body>
</html>