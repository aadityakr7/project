<html>
	<head>
		<title>Login page</title>
		<link rel="stylesheet" href="./../../../assets/stylesheets/style.css" type="text/css" />
	</head>
	<body>
		<?php include_once("./../../includes/header.php"); ?>
		<fieldset>
			<legend>Login</legend>
			<form method="POST" action="./../../controllers/login/login_handler.php">
				<table cellspacing="5px">
					<tr>
						<td>Name: </td>
						<td><input type="text" name="uname" required="required" /></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><input type="password" name="password" required="required" /></td>
					</tr>
					<tr>
						<td align="right" colspan="2"><input type="submit" value="Login" /></td>
					</tr>
				</table>
			</form>
			<a href="fgotpass.php">Forgot password?</a>
			<a href="register.php">Sign up here</a>
		</fieldset>
	</body>
</html>


<?php include_once("./../../includes/footer.php"); ?>