<?php
/* This file contains all the user-defined functions*/

class UserFunctions
{
	private $db_conn;

	function __construct($db)
	{
		// constructor
		$this->db_conn = $db;
	}

	// For checking if the query is executed successfully
	public function confirm_query($result_set)
	{
		if (!$result_set) {
			die("Query execution failed" . $this->db_conn->error());
		}
	}

	// For redirecting user to a certain location
	public function redirect_to($location = NULL)
	{
		if ($location != NULL) {
			header("Location: {$location}");
			exit();
		}
	}

	// To check if the user exists or not
	public function user_exists($uname)
	{
		//$db1 = new MySQLi('localhost','root','root','social_project');
		$qry = "select uname from users where uname='$uname'";
		$res = $this->db_conn->query($qry);
		$this->confirm_query($res);
		$row = $res->fetch_assoc();
		if ($row > 0) {
			return true;
		}else {
			return false;
		}
	}
	
}

?>